package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  List<List<Color>>grid;
  int rows;
  int cols;

  public ColorGrid(int rows, int cols){

    this.rows = rows;
    this.cols = cols;

    this.grid = new ArrayList<List<Color>>();

  for (int i = 0; i < this.rows; i++) {
    List<Color> row = new ArrayList<>();
    this.grid.add(row);
    for (int j = 0; j < this.cols; j++) {
      row.add(null);
    }
  }
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> colorPosList = new ArrayList<>();
    for (int r = 0; r < rows; r++){
      List<Color> row = grid.get(r);
      for (int c = 0; c < cols; c++){
        Color color = row.get(c);
        CellPosition cellPosition = new CellPosition(r, c);
        CellColor cellColor = new CellColor(cellPosition, color);
        colorPosList.add(cellColor);
      }
    }
    return colorPosList;
  }

  @Override
  public Color get(CellPosition pos) {
    List<Color> row = grid.get(pos.row());
    return row.get(pos.col());
  }

  @Override
  public void set(CellPosition pos, Color color) {
    List<Color> row = grid.get(pos.row());
    row.set(pos.col(), color);
  }
}