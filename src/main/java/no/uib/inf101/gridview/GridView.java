package no.uib.inf101.gridview;
import no.uib.inf101.colorgrid.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JPanel;

public class GridView extends JPanel {
  private static final int defaultWidth = 400;
  private static final int defaultHeight = 300;
  private final IColorGrid colorGrid;
  private static final Color marginColor = Color.LIGHT_GRAY;
  private static final double margin = 30.0;

  public GridView(IColorGrid colorGrid) {
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(defaultWidth, defaultHeight));

  } 

  private static void drawCells(Graphics2D g2, CellColorCollection colorCollection, CellPositionToPixelConverter cellposToPixel){
    List<CellColor> cells = colorCollection.getCells();

    for (CellColor cellColor : cells) {
      CellPosition cellPosition = cellColor.cellPosition();
      Rectangle2D newRectangle = cellposToPixel.getBoundsForCell(cellPosition);

      Color color = cellColor.color();
      if (color == null) {
        g2.setColor(Color.DARK_GRAY);
        g2.fill(newRectangle);

      } else {
        g2.setColor(color);
        g2.fill(newRectangle);

      }
    }
  }

  private void drawGrid(Graphics2D g2) {
    double x = margin;
    double y = margin;
    double width = this.getWidth() - 2 * margin;
    double height = this.getHeight() - 2 * margin;
    Rectangle2D marginRect = new Rectangle2D.Double(x, y, width, height);

    g2.setColor(marginColor);
    g2.fill(marginRect);

    drawCells(g2, colorGrid, new CellPositionToPixelConverter(marginRect,colorGrid, margin));
}

  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);
  }
}
